import { Request, Response, NextFunction } from 'express';
import { Model, Document } from 'mongoose';

export const generateGetAllController = (model: Model<Document>) => (
  req: Request,
  res: Response,
  next: NextFunction
) =>
  model
    .find({})
    .then(docs => res.status(200).json(docs))
    .catch(err => next(err));

export const generateFindOneController = (model: Model<Document>) => ({
  res,
  next,
  searchCriteria,
  fallbackObject
}: {
  req: Request;
  res: Response;
  next: NextFunction;
  searchCriteria: { [key: string]: any };
  fallbackObject: any;
}) =>
  model
    .findOne(searchCriteria)
    .then(doc => {
      const result = doc || fallbackObject;
      return res.status(200).json(result);
    })
    .catch(err => next(err));

export const generateCreateOneController = (model: Model<Document>) => (
  req: Request,
  res: Response,
  next: NextFunction
) =>
  model
    .create(req.body)
    .then(doc => res.status(201).json(doc))
    .catch(err => next(err));
