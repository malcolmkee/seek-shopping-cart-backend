const isNil = (value: any) => typeof value === 'undefined' || value === null;
const isFilledText = (value: any) => typeof value === 'string' && value.length > 0;

export const is = {
  FilledText: isFilledText,
  Nil: isNil
};
