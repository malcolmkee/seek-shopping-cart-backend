/**
 * Discount type that applies for specific customer
 */
export enum CustomerDiscountType {
  PriceOverride,
  BulkPriceOverride,
  BulkBonusPack
}

export interface IPriceOverrideDiscount {
  type: CustomerDiscountType.PriceOverride;
  discountedPrice: number;
}

export interface IBulkPriceOverrideDiscount {
  type: CustomerDiscountType.BulkPriceOverride;
  discountedPrice: number;
  minQty: number;
}

export interface IBulkBonusPackDiscount {
  type: CustomerDiscountType.BulkBonusPack;
  packSize: number;
  freeQty: number;
}

export type ICustomerDiscount =
  | IPriceOverrideDiscount
  | IBulkPriceOverrideDiscount
  | IBulkBonusPackDiscount;

/**
 * Discount information for ads type
 */
export interface IAdsDiscount {
  adsTypeId: string;
  discount: ICustomerDiscount;
}

export interface ICustomerCustomPricing {
  customerId: number;
  discounts: IAdsDiscount[];
}

export interface IAdsType {
  id: string;
  name: string;
  basePrice: number;
}
