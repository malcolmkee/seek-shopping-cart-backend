import { RequestHandler } from 'express';
import {
  generateCreateOneController,
  generateFindOneController,
  generateGetAllController
} from '../../../util/controllerHelper';
import { AdsType, CustomerPricing, CustomerPricingV2 } from './ads.model';

export const getAdsTypes = generateGetAllController(AdsType);

export const createAdsTypes = generateCreateOneController(AdsType);

const getCustomerPricingController = generateFindOneController(CustomerPricing);

export const getCustomerCustomPricing: RequestHandler = (req, res, next) => {
  const { customerId } = req.query;

  return getCustomerPricingController({
    req,
    res,
    next,
    searchCriteria: { customerId },
    fallbackObject: { customerId: Number(customerId), discounts: [] }
  });
};

const getCustomerPricingControllerV2 = generateFindOneController(CustomerPricingV2);

export const getCustomerCustomPricingV2: RequestHandler = (req, res, next) => {
  const { customerId } = req.query;

  return getCustomerPricingControllerV2({
    req,
    res,
    next,
    searchCriteria: { customerId },
    fallbackObject: { customerId, discounts: [] }
  });
};

export const createCustomerCustomPricing = generateCreateOneController(CustomerPricingV2);
