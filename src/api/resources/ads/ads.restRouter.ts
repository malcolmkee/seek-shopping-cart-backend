import { Router } from 'express';
import * as adsController from './ads.controller';

export const adsRouter = Router();

adsRouter
  .route('/types')
  .get(adsController.getAdsTypes)
  .post(adsController.createAdsTypes);

adsRouter.route('/customerPricing').get(adsController.getCustomerCustomPricing);

adsRouter
  .route('/customerPricingV2')
  .get(adsController.getCustomerCustomPricingV2)
  .post(adsController.createCustomerCustomPricing);
