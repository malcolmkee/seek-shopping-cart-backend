import { model, Schema } from 'mongoose';
import { BaseSchema } from '../../baseSchema';

const adsTypeSchema = new BaseSchema({
  idKey: {
    type: String,
    unique: true,
    required: [true, 'Ads Type must have id key']
  },
  name: {
    type: String,
    required: [true, 'Ads Type must have a name']
  },
  basePrice: {
    type: Number,
    default: 0
  }
});

export const AdsType = model('adstype', adsTypeSchema, 'adstypes');

const customerDiscountSchema = new BaseSchema({
  type: Number,
  discountedPrice: Number,
  minQty: Number,
  packSize: Number,
  freeQty: Number
});

const adsDiscountSchema = new BaseSchema({
  adsTypeId: {
    type: String,
    required: [true, 'Ads Discount must have ads type id']
  },
  discount: {
    type: customerDiscountSchema,
    required: [true, 'Ads Discount must have discount details']
  }
});

const customerPricingSchema = new BaseSchema({
  customerId: {
    type: Number,
    unique: true,
    required: [true, 'Customer Pricing must have customer id']
  },
  discounts: [adsDiscountSchema]
});

export const CustomerPricing = model(
  'adscustomerpricing',
  customerPricingSchema,
  'adscustomerpricings'
);

const customerPricingSchemaV2 = new BaseSchema({
  customerId: {
    type: Schema.Types.ObjectId,
    ref: 'customer'
  },
  discounts: [adsDiscountSchema]
});

export const CustomerPricingV2 = model(
  'adscustomerpricingV2',
  customerPricingSchemaV2,
  'adscustomerpricingsV2'
);
