import { model } from 'mongoose';
import { BaseSchema } from '../../baseSchema';

const customerSchema = new BaseSchema({
  name: {
    type: String,
    required: [true, 'Customer must have a name']
  }
});

export const Customer = model('customer', customerSchema, 'customers');
