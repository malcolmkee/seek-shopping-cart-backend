import {
  generateCreateOneController,
  generateGetAllController,
  generateFindOneController
} from '../../../util/controllerHelper';
import { Customer } from './customer.model';

export const getCustomers = generateGetAllController(Customer);

export const createCustomer = generateCreateOneController(Customer);
