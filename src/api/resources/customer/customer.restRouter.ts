import { Router } from 'express';
import * as customerController from './customer.controller';

export const customerRouter = Router();

customerRouter
  .route('/')
  .get(customerController.getCustomers)
  .post(customerController.createCustomer);
