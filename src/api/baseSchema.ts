import { Schema, SchemaDefinition, SchemaOptions } from 'mongoose';

export class BaseSchema extends Schema {
  constructor(def?: SchemaDefinition, options?: SchemaOptions) {
    super(def, options);

    this.virtual('id').get(function() {
      return this._id && this._id.toHexString();
    });

    this.set('toJSON', { virtuals: true });
  }
}
