import { Router } from 'express';
import { adsRouter } from './resources/ads/ads.restRouter';
import { customerRouter } from './resources/customer/customer.restRouter';
import { apiErrorHandler } from './errorHandler';

export const restRouter = Router();

restRouter.use('/ads', adsRouter);
restRouter.use('/customer', customerRouter);

restRouter.use(apiErrorHandler);
