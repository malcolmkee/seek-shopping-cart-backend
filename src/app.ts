import express from 'express';

import { restRouter } from './api/restRouter';
import { connect } from './db';
import { setupMiddleware } from './middleware';

export const app = express();
connect();
setupMiddleware(app);

app.use('/api', restRouter);

app.use('/', (req, res) => res.status(200).json({ message: 'Welcome!' }));
