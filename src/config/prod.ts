export const prodConfig = {
  port: process.env.PORT,
  db: {
    url: process.env.MONGOLAB_URI
  }
};
