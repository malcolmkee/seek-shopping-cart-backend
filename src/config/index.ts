import merge from 'lodash.merge';
import { devConfig } from './dev';
import { testConfig } from './testing';
import { prodConfig } from './prod';

process.env.NODE_ENV = process.env.NODE_ENV || 'development';

const env = process.env.NODE_ENV;

const baseConfig = {
  port: 4000,
  db: {
    url: 'mongodb://localhost/seek-cart'
  }
};

let envConfig = {};

switch (env) {
  case 'development':
  case 'dev':
    envConfig = devConfig;
    break;

  case 'testing':
  case 'test':
    envConfig = testConfig;
    break;

  case 'production':
  case 'prod':
    envConfig = prodConfig;
    break;

  default:
    envConfig = devConfig;
}

export const config = merge(baseConfig, envConfig);
