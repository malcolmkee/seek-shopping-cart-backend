# SEEK Shopping Cart Backend

Backend for Job Advertisement Checkout System.

[Demo](https://seek-cart.herokuapp.com/)

## Api endpoints:

1. `/api/ads/types`: api to get the base information of advertisement type
2. `/api/customer`: api to get/create customer
3. `/api/customerPricingV2`: api to get the custom pricing for the customer. Expecting query params: { customerId }

## Deprecated

1. `/api/customerPricing`: api to get the custom pricing for the customer. Expecting query params: { customerId }
